﻿using System;

namespace WeatherModel
{
    public class WeatherResponse
    {
        public int Status { get; set; }
        public string Msg { get; set; }
        public WeatherSummary Result { get; set; }
    }
}
