using System;
namespace WeatherModel
{
    public class Hourly
    {
        public string Time { get; set; }
        public string Weather { get; set; }
        public int Temp { get; set; }
        public int Img { get; set; }
    }
}
