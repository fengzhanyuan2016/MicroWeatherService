﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WeatherModel
{
    public class WeatherSummary
    {
        public string City { get; set; }
        public int CityId { get; set; }
        public string CityCode { get; set; }
        public DateTime Date { get; set; }
        public string Weather { get; set; }
        public int Temp { get; set; }
        public List<Index> Index { get; set; }
        public List<Hourly> Hourly { get; set; }

    }
}
