﻿using MessagePack;
using System;

namespace WeatherConsulModel
{

        [MessagePackObject(true)]
        public class User
        {
            public string Name { get; set; }
            public int Sid { get; set; }
        }
    
}
