﻿using InfluxDB.Collector;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherConsulService
{
    public static class ServiceBuilderExtensions
    {
        public static IServiceCollection AddInfluxDb(this IServiceCollection services)
        {
            Metrics.Collector = new CollectorConfiguration()
                                .Tag.With("host", Environment.GetEnvironmentVariable("COMPUTERNAME"))
                                .Batch.AtInterval(TimeSpan.FromSeconds(2))
                                .WriteTo.InfluxDB("http://192.168.1.101:8086", "cadvisor")
                                .CreateCollector();


            return services;
        }



    }
}
