﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherConsulService.Model
{
    public class ConsulRegisterOption
    {
        public string ConsulIP { get; set; }
        public int ConsulPort { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        public string ServiceName { get; set; }
    }
}
