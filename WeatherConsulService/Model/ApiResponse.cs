﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherConsulService.Model
{
    [MessagePackObject(true)]
    public class ApiResponse<T>
    {
        public string Msg;
        public int Status;
        public T Data;
        public static ApiResponse<T> SUCCESS(T t) {
            return new ApiResponse<T>
            {
                Msg = "Success",
                Status = 0,
                Data = t
            };
        }
    }
}
