﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WeatherConsulService.Model;
using Swashbuckle;
using System.IO;
using Microsoft.Extensions.PlatformAbstractions;
using MagicOnion.Server;
using Grpc.Core;
using MagicOnion;
using Swashbuckle.AspNetCore.Swagger;
using WeatherConsulService.Service;
using WeatherConsulInterface;

namespace WeatherConsulService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {

            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<IUserService, UserService>();
            services.AddInfluxDb();

            //这代码跟咱们之前定义服务的那个代码一个样子
            var service = MagicOnionEngine.BuildServerServiceDefinition(new MagicOnionOptions(true)
            {
                MagicOnionLogger = new MagicOnionLogToGrpcLogger()
            });
            var server = new Server
            {
                Services = { service },
                Ports = { new ServerPort("localhost", 23732, ServerCredentials.Insecure) }
            };


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "API", Version = "v1" });
            });
            server.Start();            //这里添加服务
            services.Add(new ServiceDescriptor(typeof(MagicOnionServiceDefinition), service));
            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }            
            //以下是swagger的用法，不赘述
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API-v1");
            });

            app.UseMvc();
            app.UseRegisterConsul();//here
            app.UseInfluxDb();
        }
    }
}
