﻿using Grpc.Core;
using MagicOnion.Client;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherConsulService.Service;
using WeatherConsulInterface;
using InfluxDB.Collector;


namespace WeatherConsulService.Controllers
{
    [Produces("application/json")]
    [Route("api/user")]
    public class UserController:Controller
    {
        private IUserService service;
        public UserController(IUserService userService) {
            this.service = userService;
        }
        
        [HttpGet]
        public async Task<IActionResult> Get() {

            var user = await service.GetStudent(1);
            return Json(user);
        }

        [Route("rpc")]
        public async Task<IActionResult> GetWithRpcClientAsync() {

            // standard gRPC channel
            var channel = new Channel("localhost", 23732, ChannelCredentials.Insecure);

            // create MagicOnion dynamic client proxy
            var client = MagicOnionClient.Create<IUserService>(channel);

            // call method.
            var result = await client.GetStudent(1);
            return Json(result);
        }

    }
}
