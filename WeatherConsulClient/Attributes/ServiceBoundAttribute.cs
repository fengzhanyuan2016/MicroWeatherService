﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherConsulClient.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ServiceBoundAttribute:Attribute
    {
        public string ServiceName { get; set; }
        public string Tag { get; set; }
    }
}
