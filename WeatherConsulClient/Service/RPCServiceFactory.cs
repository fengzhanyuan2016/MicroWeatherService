﻿using Consul;
using Grpc.Core;
using MagicOnion;
using MagicOnion.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WeatherConsulClient.Attributes;
using WeatherConsulClient.Model;

namespace WeatherConsulClient.Service
{


    public interface IRPCServiceFactory
    {
        [ServiceBound(ServiceName ="",Tag ="")]
        T GetService<T>(ServiceOption option) where T : IService<T>;





    }
    public class RPCServiceFactory: IRPCServiceFactory
    {
        public T GetService<T>(ServiceOption option) where T : IService<T>
        {      
            var conculClient = new ConsulClient(x => x.Address = new Uri($"http://127.0.0.1:8500"));//请求注册的 Consul 地址
            //获取服务接口实现内部的调用
            var weatherService = conculClient.Catalog.Service(option.Name, option.Tag).Result;
            if (weatherService.StatusCode == HttpStatusCode.OK && weatherService.Response.Any())
            {
                var service = weatherService.Response.First();
                //获取服务的策略
                var channel = new Channel(service.Address, service.ServicePort + 1, ChannelCredentials.Insecure);
                return GetClient<T>(channel);
            }
            return default(T);
        }
        private T GetClient<T>(Channel channel) where T : IService<T>
        {
            return MagicOnionClient.Create<T>(channel);
        }

    }
}
