﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using MagicOnion.Client;
using MagicOnion;

namespace WeatherConsulClient.Controllers
{
    public class BaseController:Controller
    {
        public T GetClient<T>(Channel channel) where T : IService<T>
        {
            return MagicOnionClient.Create<T>(channel);
        }
    }
}
