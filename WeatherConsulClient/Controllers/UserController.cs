﻿using Grpc.Core;
using MagicOnion.Client;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherConsulClient.Model;
using WeatherConsulClient.Service;
using WeatherConsulInterface;

namespace WeatherConsulClient.Controllers
{
    [Produces("application/json")]
    [Route("api/user")]
    public class UserController : BaseController
    {
        private IUserService client;
        private IRPCServiceFactory serviceFactory;
        public UserController(Channel channel, IRPCServiceFactory serviceFactory) {
            this.client = GetClient<IUserService>(channel);
            this.serviceFactory = serviceFactory;
        }
        [Route("rpc")]
        public async Task<IActionResult> GetWithRpcClientAsync()
        {
            // call method.
            var result = await client.GetStudent(1);
            return Json(result);
        }
        [Route("rpc2")]
        public async Task<IActionResult> GetWithRpcClientA()
        {
            // call method.
            ServiceOption option = new ServiceOption {Name= "Weather.Service", Tag= "1.0-/Weather.Service" };
            var result = serviceFactory.GetService<IUserService>(option).GetStudent(1);
            return Json(result);
        }


    }


}
