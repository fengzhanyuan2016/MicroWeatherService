﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherConsulClient.Model
{
    public class ServiceOption
    {
        public string Name { get; set; }
        public string Tag { get; set; }
        public string ConsulIP { get; set; }
        public int ConsulPort { get; set; }

    }
}
