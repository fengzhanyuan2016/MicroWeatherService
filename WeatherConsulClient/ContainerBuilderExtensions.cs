﻿using Consul;
using Grpc.Core;
using MagicOnion;
using MagicOnion.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WeatherConsulClient.Model;
using WeatherConsulInterface;

namespace WeatherConsulClient
{
    public static class ContainerBuilderExtensions
    {

        public static IApplicationBuilder RegisterConsul(this IApplicationBuilder app, IApplicationLifetime lifetime, ServiceEntity serviceEntity)
        {
            var  conculClient = new ConsulClient(x => x.Address = new Uri($"http://{serviceEntity.ConsulIP}:{serviceEntity.ConsulPort}"));//请求注册的 Consul 地址
            var httpCheck = new AgentServiceCheck()
            {
                DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(5),//服务启动多久后注册
                Interval = TimeSpan.FromSeconds(10),//健康检查时间间隔，或者称为心跳间隔
                HTTP = $"http://{serviceEntity.IP}:{serviceEntity.Port}/api/health",//健康检查地址
                Timeout = TimeSpan.FromSeconds(5)
            };            // Register service with consul
            var registration = new AgentServiceRegistration()
            {
                Checks = new[] { httpCheck },
                ID = Guid.NewGuid().ToString(),
                Name = serviceEntity.ServiceName,
                Address = serviceEntity.IP,
                Port = serviceEntity.Port,
                Tags = new[] { $"urlprefix-/{serviceEntity.ServiceName}" }//添加 urlprefix-/servicename 格式的 tag 标签，以便 Fabio 识别 };
            };

            conculClient.Agent.ServiceRegister(registration).Wait();//服务启动时注册，内部实现其实就是使用 Consul API 进行注册（HttpClient发起）

            lifetime.ApplicationStopping.Register(() =>
            {
                conculClient.Agent.ServiceDeregister(registration.ID).Wait();//服务停止时取消注册 }); return app;
            });
            return app;
        }
        

        public static IServiceCollection AddRpc(this IServiceCollection services, ServiceOption option)
        {
            var conculClient = new ConsulClient(x => x.Address = new Uri($"http://{option.ConsulIP}:{option.ConsulPort}"));//请求注册的 Consul 地址
            //获取服务接口实现内部的调用
            var weatherService = conculClient.Catalog.Service(option.Name, option.Tag).Result;
            if (weatherService.StatusCode == HttpStatusCode.OK&& weatherService.Response.Any())
            {
                var service = weatherService.Response.First();
                //获取服务的策略
                var channel = new Channel(service.Address, service.ServicePort+1, ChannelCredentials.Insecure);
                services.AddSingleton(channel);
            }

            return services;
        }



    }
}
