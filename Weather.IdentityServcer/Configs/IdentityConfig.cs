﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Weather.IdentityServer.Configs
{
    public class IdentityConfig
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("gateway","API网关"),
                new ApiResource("api_user","API用户服务"),
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
                {
                    new Client
                    {
                        ClientId = "client",
                        AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                        ClientSecrets = { new Secret("secret".Sha256()) },
                        Claims = { new Claim("name","名称") },
                        AllowedScopes = { "gateway" }
                    },

                    new Client
                    {
                        ClientId = "client1",
                        AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                        ClientSecrets = { new Secret("secret1".Sha256()) },
                        Claims = { new Claim("name","名称") },
                        AllowedScopes = { "api_user" }
                    },

                };
        }

        public static IEnumerable<TestUser> Users()
        {
            return new[]
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "mail@qq.com",
                    Password = "password"
                }
            };
        }

    }
}
