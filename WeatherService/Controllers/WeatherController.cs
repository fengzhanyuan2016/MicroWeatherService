﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WeatherModel;
using WeatherService.Configs;
using WeatherService.Filter;
using WebApiClient;
namespace WeatherService.Controllers
{
    [Route("[controller]")]
    public class WeatherController : BaseController
    {
        private IOptions<User> _configUser;
        public WeatherController(IOptions<AppSettings> setting,IOptions<User> configUser) : base(setting)
        {
            base.appSettings = setting.Value;
            _configUser=configUser;
        }
        // GET api/values
        [HttpGet("info")]
        public string Get()
        {
            var user=_configUser.Value;
            return user.Name + "\r\n" + user.Age + "\r\n" + appSettings.TimeOut;
        }

        // GET api/weather/上海
        [HttpGet("city/{name}")]
        public IActionResult GetDataByCityName(string name)
        {
            using (proxy)
            {         
                var result = proxy.GetDataByCityName(name).InvokeAsync().Result;
                return Json(result);
            }
        }
        
        // POST api/values
        [HttpGet("code/{code}")]
        public IActionResult GetDataByCityCode(string code)
        {
            using (proxy)
            {         
                var result = proxy.GetDataByCityCode(code).InvokeAsync().Result;
                return Json(result);
            }
        }
    }
}
