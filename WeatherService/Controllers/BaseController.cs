﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherService.Configs;
using WeatherService.Filter;
using WebApiClient;

namespace WeatherService.Controllers
{
    public class BaseController:Controller
    {
        protected IAliWeatherProxy proxy;
        protected AppSettings appSettings;
        public BaseController(IOptions<AppSettings> setting)
        {
            this.appSettings = setting.Value;
            var apiConfig = new HttpApiConfig
            {
                // 请求的域名，会覆盖[HttpHost]特性
                HttpHost = new Uri(appSettings.Host),
            };
            apiConfig.HttpClient.Timeout =TimeSpan.FromMilliseconds(appSettings.TimeOut);
            apiConfig.GlobalFilters.Add(new GlobalFilter(appSettings.Authorization));
            this.proxy = HttpApiClient.Create<IAliWeatherProxy>(apiConfig);
        }
    }
}
