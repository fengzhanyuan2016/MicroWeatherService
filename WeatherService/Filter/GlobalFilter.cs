﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebApiClient;
using WebApiClient.AuthTokens;
using WebApiClient.Contexts;

namespace WeatherService.Filter
{
    public class GlobalFilter:IApiActionFilter
    {
        public string Token { get; private set; }
        public GlobalFilter(string token) {
            this.Token = token;
        }

        public Task OnBeginRequestAsync(ApiActionContext context)
        {   
            context.RequestMessage.Headers.TryAddWithoutValidation("Authorization", Token);
            return Task.CompletedTask;
        }

        public Task OnEndRequestAsync(ApiActionContext context)
        {
            return Task.CompletedTask;
        }
    }
}
