﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherService.Configs
{
    public class AppSettings
    {
        public int TimeOut { get; set; }
        public string Host { get; set; }
        public string Authorization { get; set; }
    }
}
