using System;
using Microsoft.Extensions.Options;
using WeatherModel;
using WebApiClient;
using WebApiClient.Attributes;

namespace WeatherService
{
    //[HttpHost("http://jisutqybmf.market.alicloudapi.com")]
    //[Header("Authorization", "APPCODE 70c6bc2c90ae456c8d82291b518376b8")]
    [JsonReturn]
    public interface IAliWeatherProxy:IHttpApi
    {
        [HttpGet("/weather/query")]
        ITask<WeatherResponse>GetDataByCityName(string city);
        [HttpGet("/weather/query")]
        ITask<WeatherResponse>GetDataByCityCode(string citycode);
    } 



}

