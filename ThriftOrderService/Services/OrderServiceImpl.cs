﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts;
using static Contracts.OrderService;

namespace ThriftOrderService.Services
{
    public class OrderServiceImpl : Iface
    {
        public InvokeResult Delete(int orderId)
        {
            return new InvokeResult {Code=ResponseCode.SUCCESS,Message= "SUCCESS" };
        }

        public Order Get(int orderId)
        {
            return new Order {
                Amount=100,
                OrderId=100,
                Remark="test",
                SkuId=888888
            };
        }
    }
}
