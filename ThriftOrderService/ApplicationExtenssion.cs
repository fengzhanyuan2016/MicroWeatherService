﻿using Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thrift.Server;
using Thrift.Transport;
using ThriftOrderService.Services;
using static Contracts.OrderService;

namespace ThriftOrderService
{
    public static class ApplicationExtenssion
    {
        public static IApplicationBuilder UseThriftServer(this IApplicationBuilder appBuilder)
        {
            var orderService = new OrderServiceImpl();
            Processor processor = new Processor(orderService);

            TServerTransport transport = new TServerSocket(8800);
            TServer server = new TThreadPoolServer(processor, transport);

            var services = appBuilder.ApplicationServices.CreateScope().ServiceProvider;

            var lifeTime = services.GetService<IApplicationLifetime>();
            lifeTime.ApplicationStarted.Register(() =>
            {
                server.Serve();
            });
            lifeTime.ApplicationStopped.Register(() =>
            {
                server.Stop();
                transport.Close();
            });

            return appBuilder;
        }
    }
}
