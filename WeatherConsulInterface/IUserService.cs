﻿using MagicOnion;
using System;
using WeatherConsulModel;

namespace WeatherConsulInterface
{
    public interface IUserService : IService<IUserService>
    {
        UnaryResult<User> GetStudent(int sid);
    }
}
