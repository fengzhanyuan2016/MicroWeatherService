﻿using Contracts;
using System;
using Thrift.Protocol;
using Thrift.Transport;

namespace ThriftOrderClient
{
    class Program
    {
        static void Main(string[] args)
        {
            TTransport transport = new TSocket("localhost", 8800);
            TProtocol protocol = new TBinaryProtocol(transport);
            var client = new OrderService.Client(protocol);
            transport.Open();

            var createResult = client.Delete(100);
            var order = client.Get(10);
            Console.WriteLine(order.Remark);
            transport.Close();
            Console.ReadKey();
        }
    }
}
