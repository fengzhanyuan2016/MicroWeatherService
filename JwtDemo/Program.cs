﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Newtonsoft.Json;
using System.Text;
using System.Linq;
namespace JwtDemo
{
    class Program
    {

        static void Main(string[] args)
        {

            JwtSettings setting = new JwtSettings
            {
                Issuer = "",
                Audience = "",
                SecretKey = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1"
            };
            //key
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(setting.SecretKey));

            // credentials
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //header
            var header = new JwtHeader(credentials);


            //payload
            var payload = new JwtPayload
           {
                { "some ", "hello "},
                { "scope", "http://dummy.com/"},
                {"iss", "http://example.com/"},
                {"sub", "yosida95"},
                {"iat", 1534906642 },
                {"exp", 1534906800 }
           };


            var secToken = new JwtSecurityToken(header, payload);

            var handler = new JwtSecurityTokenHandler();
            var tokenString = handler.WriteToken(secToken);
            Console.WriteLine(tokenString);

            //解密

            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.hDp0nlapOieGSVJdJDLLN_BvOmDbfAimZttbc5L5fnw";
            var handler2 = new JwtSecurityTokenHandler();
            if (handler2.CanReadToken(tokenString))
            {
                TokenValidationParameters parameters = new TokenValidationParameters();
                parameters.IssuerSigningKey = key;


                SecurityToken securityToken = null;
                try
                {
                    handler2.ValidateToken(tokenString, parameters, out securityToken);
                }

                catch (SecurityTokenExpiredException ex)
                {
                    throw ex;
                }

                Console.WriteLine(securityToken.SecurityKey);

            }






            var data = handler2.ReadJwtToken(token);


            string payloadStr = JsonConvert.SerializeObject(data.Payload);
            Console.WriteLine(payloadStr);





            Console.ReadKey();


        }
    }
    public class JwtSettings
    {
        /// <summary>
        /// 证书颁发者
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 允许使用的角色
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// 加密字符串
        /// </summary>
        public string SecretKey { get; set; }
    }
}
